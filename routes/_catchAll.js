const express = require('express');
const router = express.Router();

const fs = require('fs');

const REGEX_CURL_USER_AGENT = /^curl\//

/* GET home page. */
router.use(function(req, res, next) {
    const isSecure = req.secure
    const isCurl = REGEX_CURL_USER_AGENT.test(req.get('User-Agent'))
    const config = JSON.parse(fs.readFileSync("config.json"))
    const redirectUrl = config['dotfiles']['repo'] + req.originalUrl

    const curlUrl = 'https://' + req.get('host')
    
    // Redirect to dotfiles repo is not `curl`
    if (!isCurl) {
        res.status(302)
            .header({"Location": redirectUrl})
    }

    // Redirect to secure curl URL if curl is used without https
    if (isCurl && !isSecure) {
        res.status(302)
            .header({"Location": curlUrl})
    }

    res.render('dotfiles', {
        secure: req.secure,
        dotfiles: config['dotfiles'],
        curlUrl
    })
});

module.exports = router;

